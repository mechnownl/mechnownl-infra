# global:
#   imageRegistry: ""
#   imagePullSecrets: []
#   storageClass: "nfs"

# # auth:
# #   clientProtocol: plaintext
# #   interBrokerProtocol: plaintext

# # externalAccess:
# #   enabled: true
# #   service:
# #     type: LoadBalancer
# #     port: 9092
# #   autoDiscovery:
# #     enabled: true

# # broker:
# #   automountServiceAccountToken: true

# # controller:
# #   automountServiceAccountToken: true

# service:
#   type: ClusterIP
#   port: 9092
#   targetPort: 9092
#   nodePort: null
#   externalPort: null

# listeners:
#   ## @param listeners.client.name Name for the Kafka client listener
#   ## @param listeners.client.containerPort Port for the Kafka client listener
#   ## @param listeners.client.protocol Security protocol for the Kafka client listener. Allowed values are 'PLAINTEXT', 'SASL_PLAINTEXT', 'SASL_SSL' and 'SSL'
#   ## @param listeners.client.sslClientAuth Optional. If SASL_SSL is enabled, configure mTLS TLS authentication type. If SSL protocol is enabled, overrides tls.authType for this listener. Allowed values are 'none', 'requested' and 'required'
#   client:
#     containerPort: 9092
#     protocol: PLAINTEXT
#     name: CLIENT
#     sslClientAuth: ""
#   ## @param listeners.controller.name Name for the Kafka controller listener
#   ## @param listeners.controller.containerPort Port for the Kafka controller listener
#   ## @param listeners.controller.protocol Security protocol for the Kafka controller listener. Allowed values are 'PLAINTEXT', 'SASL_PLAINTEXT', 'SASL_SSL' and 'SSL'
#   ## @param listeners.controller.sslClientAuth Optional. If SASL_SSL is enabled, configure mTLS TLS authentication type. If SSL protocol is enabled, overrides tls.authType for this listener. Allowed values are 'none', 'requested' and 'required'
#   ## Ref: https://cwiki.apache.org/confluence/display/KAFKA/KIP-684+-+Support+mutual+TLS+authentication+on+SASL_SSL+listeners
#   controller:
#     name: CONTROLLER
#     containerPort: 9093
#     protocol: PLAINTEXT
#     sslClientAuth: ""
#   ## @param listeners.interbroker.name Name for the Kafka inter-broker listener
#   ## @param listeners.interbroker.containerPort Port for the Kafka inter-broker listener
#   ## @param listeners.interbroker.protocol Security protocol for the Kafka inter-broker listener. Allowed values are 'PLAINTEXT', 'SASL_PLAINTEXT', 'SASL_SSL' and 'SSL'
#   ## @param listeners.interbroker.sslClientAuth Optional. If SASL_SSL is enabled, configure mTLS TLS authentication type. If SSL protocol is enabled, overrides tls.authType for this listener. Allowed values are 'none', 'requested' and 'required'
#   interbroker:
#     containerPort: 9094
#     protocol: PLAINTEXT
#     name: INTERNAL
#     sslClientAuth: ""
#   ## @param listeners.external.containerPort Port for the Kafka external listener
#   ## @param listeners.external.protocol Security protocol for the Kafka external listener. . Allowed values are 'PLAINTEXT', 'SASL_PLAINTEXT', 'SASL_SSL' and 'SSL'
#   ## @param listeners.external.name Name for the Kafka external listener
#   ## @param listeners.external.sslClientAuth Optional. If SASL_SSL is enabled, configure mTLS TLS authentication type. If SSL protocol is enabled, overrides tls.sslClientAuth for this listener. Allowed values are 'none', 'requested' and 'required'
#   external:
#     containerPort: 9095
#     protocol: PLAINTEXT
#     name: EXTERNAL
#     sslClientAuth: ""
#   ## @param listeners.extraListeners Array of listener objects to be appended to already existing listeners
#   ## E.g.
#   ## extraListeners:
#   ##  - name: CUSTOM
#   ##    containerPort: 9097
#   ##    protocol: SASL_PLAINTEXT
#   ##    sslClientAuth: ""
#   ##
#   extraListeners: []
#   ## NOTE: If set, below values will override configuration set using the above values (extraListeners.*, controller.*, interbroker.*, client.* and external.*)
#   ## @param listeners.overrideListeners Overrides the Kafka 'listeners' configuration setting.
#   ## @param listeners.advertisedListeners Overrides the Kafka 'advertised.listener' configuration setting.
#   ## @param listeners.securityProtocolMap Overrides the Kafka 'security.protocol.map' configuration setting.
#   overrideListeners: ""
#   advertisedListeners: ""
#   securityProtocolMap: ""
# ## @section Kafka SASL parameters
# ## Kafka SASL settings for authentication, required if SASL_PLAINTEXT or SASL_SSL listeners are configured
# ##
# sasl:
#   ## @param sasl.enabledMechanisms Comma-separated list of allowed SASL mechanisms when SASL listeners are configured. Allowed types: `PLAIN`, `SCRAM-SHA-256`, `SCRAM-SHA-512`, `OAUTHBEARER`
#   ## NOTE: At the moment, Kafka Raft mode does not support SCRAM, that is why only PLAIN is configured.
#   ##
#   enabledMechanisms: PLAIN,SCRAM-SHA-256,SCRAM-SHA-512
#   ## @param sasl.interBrokerMechanism SASL mechanism for inter broker communication.
#   ##
#   interBrokerMechanism: PLAIN
#   ## @param sasl.controllerMechanism SASL mechanism for controller communications.
#   ##
#   controllerMechanism: PLAIN
#   ## Settings for oauthbearer mechanism
#   ## @param sasl.oauthbearer.tokenEndpointUrl The URL for the OAuth/OIDC identity provider
#   ## @param sasl.oauthbearer.jwksEndpointUrl The OAuth/OIDC provider URL from which the provider's JWKS (JSON Web Key Set) can be retrieved
#   ## @param sasl.oauthbearer.expectedAudience The comma-delimited setting for the broker to use to verify that the JWT was issued for one of the expected audiences
#   ## @param sasl.oauthbearer.subClaimName The OAuth claim name for the subject.
#   ##
#   oauthbearer:
#     tokenEndpointUrl: ""
#     jwksEndpointUrl: ""
#     expectedAudience: ""
#     subClaimName: "sub"
#   ## Credentials for inter-broker communications.
#   ## @param sasl.interbroker.user Username for inter-broker communications when SASL is enabled
#   ## @param sasl.interbroker.password Password for inter-broker communications when SASL is enabled. If not set and SASL is enabled for the controller listener, a random password will be generated.
#   ## @param sasl.interbroker.clientId Client ID for inter-broker communications when SASL is enabled with mechanism OAUTHBEARER
#   ## @param sasl.interbroker.clientSecret Client Secret for inter-broker communications when SASL is enabled with mechanism OAUTHBEARER. If not set and SASL is enabled for the controller listener, a random secret will be generated.
#   ##
#   interbroker:
#     user: inter_broker_user
#     password: "randomP@ss"
#     clientId: inter_broker_client
#     clientSecret: ""
#   ## Credentials for controller communications.
#   ## @param sasl.controller.user Username for controller communications when SASL is enabled
#   ## @param sasl.controller.password Password for controller communications when SASL is enabled. If not set and SASL is enabled for the inter-broker listener, a random password will be generated.
#   ## @param sasl.controller.clientId Client ID for controller communications when SASL is enabled with mechanism OAUTHBEARER
#   ## @param sasl.controller.clientSecret Client Secret for controller communications when SASL is enabled with mechanism OAUTHBEARER. If not set and SASL is enabled for the inter-broker listener, a random secret will be generated.
#   ##
#   controller:
#     user: controller_user
#     password: "randomP@ss"
#     clientId: controller_broker_client
#     clientSecret: ""
#   ## Credentials for client communications.
#   ## @param sasl.client.users Comma-separated list of usernames for client communications when SASL is enabled
#   ## @param sasl.client.passwords Comma-separated list of passwords for client communications when SASL is enabled, must match the number of client.users
#   ##
#   client:
#     users:
#       - user
#     passwords: "randomP@ss"
#   ## Credentials for Zookeeper communications.
#   ## @param sasl.zookeeper.user Username for zookeeper communications when SASL is enabled.
#   ## @param sasl.zookeeper.password Password for zookeeper communications when SASL is enabled.
#   ##
#   zookeeper:
#     user: ""
#     password: ""
#   ## @param sasl.existingSecret Name of the existing secret containing credentials for clientUsers, interBrokerUser, controllerUser and zookeeperUser
#   ## Create this secret running the command below where SECRET_NAME is the name of the secret you want to create:
#   ##       kubectl create secret generic SECRET_NAME --from-literal=client-passwords=CLIENT_PASSWORD1,CLIENT_PASSWORD2 --from-literal=inter-broker-password=INTER_BROKER_PASSWORD --from-literal=inter-broker-client-secret=INTER_BROKER_CLIENT_SECRET --from-literal=controller-password=CONTROLLER_PASSWORD --from-literal=controller-client-secret=CONTROLLER_CLIENT_SECRET --from-literal=zookeeper-password=ZOOKEEPER_PASSWORD
#   ## The client secrets are only required when using oauthbearer as sasl mechanism.
#   ## Client, interbroker and controller passwords are only required if the sasl mechanism includes something other than oauthbearer.
#   ##
#   existingSecret: ""
# rbac:
#   ## @param rbac.create Whether to create & use RBAC resources or not
#   ## binding Kafka ServiceAccount to a role
#   ## that allows Kafka pods querying the K8s API
#   ##
#   create: true

auth:
  clientProtocol: plaintext
  interBrokerProtocol: plaintext

# externalAccess:
#   enabled: true
#   service:
#     type: NodePort
#     nodePorts:
#       external: 30092

service:
  type: ClusterIP
  port: 9092

zookeeper:
  auth:
    clientProtocol: plaintext
    interBrokerProtocol: plaintext
  service:
    type: ClusterIP

rbac:
  create: true
